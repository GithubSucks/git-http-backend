# Git http backend

This is a bare minimum implementation of the git http protocol.
This is the base of another project i am working on that will be a fully featured git service like gitlab or github under a FOSS license.


## The git-http protocol
It is a very underdocumented protocol so i am planning on either making a youtube video on it or i'll write a guide.

# How2use

1. create a directory named git at the root of the project: ```mkdir git && cd git```
2. Inside the "git" directory create a user folder where a users projects will go: ```mkdir LinusTorvalds```.
3. Create a bare repo inside of the user dir: ```cd LinusTorvalds && mkdir linux.git && cd linux.git && git init --bare``` note a bare repo must end in ```.git```
4. You can now just git clone it like this: ```git clone http://0.0.0.0:4545/LinusTorvalds/linux```.

## The first rust implementation
This is the first implementation of the git http protocol in rust.
